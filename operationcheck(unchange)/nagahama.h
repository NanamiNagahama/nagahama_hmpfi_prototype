#include <mpfi.h>

typedef struct {
  mpfr_prec_t _mpfr_prec_l;
  mpfr_sign_t _mpfr_sign_l;
  mpfr_exp_t _mpfr_exp_l;
  mp_limb_t *_mpfr_d_l;

  mpfr_prec_t _mpfr_prec_r;
  mpfr_sign_t _mpfr_sign_r;
  mpfr_exp_t _mpfr_exp_r;
  mp_limb_t *_mpfr_d_r;
}__nagahama_struct;

typedef union {
  __mpfi_struct mpfi;
  __nagahama_struct nagahama;
}__nagahama_union;
