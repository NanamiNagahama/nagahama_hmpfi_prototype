{-# LANGUAGE ForeignFunctionInterface, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}

#include <mpfr.h>
#include <chsmpfr.h>
#include <mpfi.h>
#include "nagahama.h"

module MPFI.FFIhelper where

import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Storable
import Foreign.C.Types
import Foreign.C.String
import Foreign.ForeignPtr
import System.IO.Unsafe
import Data.Int
import Data.Word
import System.Environment (getArgs)
import Data.Function(on)

data RoundMode = Near | Zero | Up | Down | MPFR_RNDNA
                 deriving (Show,Read)

instance Enum RoundMode where
#if MPFR_VERSION_MAJOR == 2
    fromEnum Near        = #{const GMP_RNDN}
    fromEnum Zero        = #{const GMP_RNDZ}
    fromEnum Up          = #{const GMP_RNDU}
    fromEnum Down        = #{const GMP_RNDD}
    fromEnum MPFR_RNDNA   = #{const GMP_RNDNA}

    toEnum #{const GMP_RNDN}    = Near
    toEnum #{const GMP_RNDZ}    = Zero
    toEnum #{const GMP_RNDU}    = Up
    toEnum #{const GMP_RNDD}    = Down
    toEnum (#{const GMP_RNDNA}) = MPFR_RNDNA
#else
    fromEnum Near        = #{const MPFR_RNDN}
    fromEnum Zero        = #{const MPFR_RNDZ}
    fromEnum Up          = #{const MPFR_RNDU}
    fromEnum Down        = #{const MPFR_RNDD}
    fromEnum MPFR_RNDNA   = #{const MPFR_RNDNA}

    toEnum #{const MPFR_RNDN}    = Near
    toEnum #{const MPFR_RNDZ}    = Zero
    toEnum #{const MPFR_RNDU}    = Up
    toEnum #{const MPFR_RNDD}    = Down
    toEnum (#{const MPFR_RNDNA}) = MPFR_RNDNA
#endif
    toEnum i                    = error $ "RoundMode.toEnum called with illegal argument :" ++ show i

data MPFR = MP{ precM :: CPrecision,
                signM :: Sign,
                expM :: Exp,
                limbM :: !(ForeignPtr Limb) } deriving (Show)

instance Storable MPFR where
  sizeOf _ = #size __mpfr_struct
  alignment _ = alignment (undefined :: #{type mpfr_prec_t})
  peek = error "MPFR.peek : Not needed and not applicble"
  poke p (MP prec s e fp) = do #{poke __mpfr_struct, _mpfr_prec} p prec
                               #{poke __mpfr_struct, _mpfr_sign} p s
                               #{poke __mpfr_struct, _mpfr_exp} p e
                               withForeignPtr fp $ \p1 -> #{poke __mpfr_struct, _mpfr_d} p p1

data MPFI = MP2{ leftM :: MPFR,
                 rightM :: MPFR } deriving (Show)

instance Storable MPFI where
  sizeOf _ = #size __mpfi_struct
  alignment _ = alignment (undefined :: #{type mpfr_prec_t})
  peek = error "MPFI.peek : Not needed and not applicble"
  poke p (MP2 l r) = do #{poke __nagahama_struct, _mpfr_prec_l} p prec1
                        #{poke __nagahama_struct, _mpfr_sign_l} p s1
                        #{poke __nagahama_struct, _mpfr_exp_l} p e1
                        withForeignPtr fp1 $ \p1 -> #{poke __nagahama_struct, _mpfr_d_l} p p1

                        #{poke __nagahama_struct, _mpfr_prec_r} p prec2
                        #{poke __nagahama_struct, _mpfr_sign_r} p s2
                        #{poke __nagahama_struct, _mpfr_exp_r} p e2
                        withForeignPtr fp2 $ \p2 -> #{poke __nagahama_struct, _mpfr_d_r} p p2
                   where (MP prec1 s1 e1 fp1) = l
                         (MP prec2 s2 e2 fp2) = r

newtype Precision = Precision { runPrec :: Word } deriving (Eq, Ord, Show, Enum)

instance Num Precision where
    (Precision w) + (Precision w') = Precision $ w + w'
    (Precision w) * (Precision w') = Precision $ w * w'
    (Precision a) - (Precision b) =
        if a >= b
        then Precision (a - b)
        else error $ "instance Precision Num (-): " ++
                       "Operation would result in negative precision."
    negate = error $ "instance Precision Num negate: " ++
                       "operation would result in negative precision"
    abs = id
    signum (Precision x) = Precision . signum $ x
    fromInteger i = if i >= 0
                    then Precision . fromInteger $ i
                    else error $ "instance Precision Num fromInteger: " ++
                             "operation would result  in negative precision"


instance Real Precision where
    toRational (Precision w) = toRational w

instance Integral Precision where
    quotRem (Precision w) (Precision w') = uncurry ((,) `on` Precision) $ quotRem w w'
    toInteger (Precision w) = toInteger w

peekP :: Ptr MPFI -> ForeignPtr Limb -> ForeignPtr Limb -> IO MPFI
peekP p fp1 fp2 = do
  r11 <- #{peek __nagahama_struct, _mpfr_prec_l}p
  r12 <- #{peek __nagahama_struct, _mpfr_sign_l}p
  r13 <- #{peek __nagahama_struct, _mpfr_exp_l}p

  r21 <- #{peek __nagahama_struct, _mpfr_prec_r}p
  r22 <- #{peek __nagahama_struct, _mpfr_sign_r}p
  r23 <- #{peek __nagahama_struct, _mpfr_exp_r}p
  
  return (MP2(MP r11 r12 r13 fp1)(MP r21 r22 r23 fp2))

withDummym :: Precision -> (Ptr MPFI -> IO CInt) -> IO (MPFI, Int)
withDummym l f =
  do alloca $ \ptr -> do
       ls <- mpfr_custom_get_size ((fromIntegral . runPrec ) l)
       fp1 <- mallocForeignPtrBytes (fromIntegral ls)
       fp2 <- mallocForeignPtrBytes (fromIntegral ls)
       
       #{poke __nagahama_struct, _mpfr_prec_l} ptr (fromIntegral l :: CPrecision)
       #{poke __nagahama_struct, _mpfr_sign_l} ptr (1 :: Sign)
       #{poke __nagahama_struct, _mpfr_exp_l}ptr (0 :: Exp)
       withForeignPtr fp1 $ \p1 -> #{poke __nagahama_struct,_mpfr_d_l}ptr p1

       #{poke __nagahama_struct, _mpfr_prec_r} ptr (fromIntegral l :: CPrecision)
       #{poke __nagahama_struct, _mpfr_sign_r} ptr (1 :: Sign)
       #{poke __nagahama_struct, _mpfr_exp_r}ptr (0 :: Exp)
       withForeignPtr fp2 $ \p1 -> #{poke __nagahama_struct,_mpfr_d_r}ptr p1


       r2 <- f ptr
       r1 <- peekP ptr fp1 fp2
       return(r1,fromIntegral r2)

expZero :: Exp
expZero = #const __MPFR_EXP_ZERO

type Prec = #type mpfr_prec_t
type Sign = #type mpfr_sign_t
type Exp  = #type mp_exp_t
type Limb = #type unsigned int

foreign import ccall unsafe "mpfi_set_str" c_mpfi_set_str ::
        Ptr MPFI -> CString -> CInt -> IO CInt
foreign import ccall unsafe "mpfr_get_str" c_mpfr_get_str ::
        CString -> Ptr Exp -> CInt -> CUInt -> Ptr MPFR -> CRoundMode -> IO CString
foreign import ccall unsafe "mpfr_custom_get_size_wrap" mpfr_custom_get_size ::
        CPrecision -> IO #{type size_t}
foreign import ccall unsafe "mpfr_free_str" c_mpfr_free_str ::
        CString -> IO ()
foreign import ccall unsafe "mpfi_add" c_mpfi_add ::
        Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_add_ui" c_mpfi_add_ui ::
        Ptr MPFI -> Ptr MPFI -> CULong -> IO CInt
foreign import ccall unsafe "mpfi_add_si" c_mpfi_add_si ::
        Ptr MPFI -> Ptr MPFI -> CLong -> IO CInt
foreign import ccall unsafe "mpfi_add_d" c_mpfi_add_d ::
        Ptr MPFI -> Ptr MPFI -> CDouble ->IO CInt
foreign import ccall unsafe "mpfi_sub" c_mpfi_sub ::
        Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_sub_ui" c_mpfi_sub_ui ::
        Ptr MPFI -> Ptr MPFI -> CULong -> IO CInt
foreign import ccall unsafe "mpfi_sub_si" c_mpfi_sub_si ::
        Ptr MPFI -> Ptr MPFI -> CLong -> IO CInt
foreign import ccall unsafe "mpfi_sub_d" c_mpfi_sub_d ::
        Ptr MPFI -> Ptr MPFI -> CDouble ->IO CInt
foreign import ccall unsafe "mpfi_ui_sub" c_mpfi_ui_sub ::
        Ptr MPFI -> CULong -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_si_sub" c_mpfi_si_sub ::
        Ptr MPFI -> CLong -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_d_sub" c_mpfi_d_sub ::
        Ptr MPFI -> CDouble -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_mul" c_mpfi_mul ::
        Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_mul_ui" c_mpfi_mul_ui ::
        Ptr MPFI -> Ptr MPFI -> CULong -> IO CInt
foreign import ccall unsafe "mpfi_mul_si" c_mpfi_mul_si ::
        Ptr MPFI -> Ptr MPFI -> CLong -> IO CInt
foreign import ccall unsafe "mpfi_mul_d" c_mpfi_mul_d ::
        Ptr MPFI -> Ptr MPFI -> CDouble ->IO CInt
foreign import ccall unsafe "mpfi_div" c_mpfi_div ::
        Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_div_ui" c_mpfi_div_ui ::
        Ptr MPFI -> Ptr MPFI -> CULong -> IO CInt
foreign import ccall unsafe "mpfi_div_si" c_mpfi_div_si ::
        Ptr MPFI -> Ptr MPFI -> CLong -> IO CInt
foreign import ccall unsafe "mpfi_div_d" c_mpfi_div_d ::
        Ptr MPFI -> Ptr MPFI -> CDouble ->IO CInt
foreign import ccall unsafe "mpfi_ui_div" c_mpfi_ui_div ::
        Ptr MPFI -> CULong -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_si_div" c_mpfi_si_div ::
        Ptr MPFI -> CLong -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_d_div" c_mpfi_d_div ::
        Ptr MPFI -> CDouble -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_sqr" c_mpfi_sqr ::
        Ptr MPFI -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_sqrt" c_mpfi_sqrt ::
        Ptr MPFI -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_cbrt" c_mpfi_cbrt ::
        Ptr MPFI -> Ptr MPFI -> IO CInt
foreign import ccall unsafe "mpfi_neg" c_mpfi_neg ::
        Ptr MPFI -> Ptr MPFI  -> IO CInt
foreign import ccall unsafe "mpfi_abs" c_mpfi_abs ::
        Ptr MPFI -> Ptr MPFI  -> IO CInt
foreign import ccall unsafe "mpfi_mul_2ui" mpfi_mul_2ui ::
        Ptr MPFI -> Ptr MPFI -> CULong -> IO CInt
foreign import ccall unsafe "mpfi_mul_2si" mpfi_mul_2si ::
        Ptr MPFI -> Ptr MPFI -> CLong -> IO CInt
foreign import ccall unsafe "mpfi_div_2ui" mpfi_div_2ui ::
        Ptr MPFI -> Ptr MPFI -> CULong -> IO CInt
foreign import ccall unsafe "mpfi_div_2si" mpfi_div_2si ::
        Ptr MPFI -> Ptr MPFI -> CLong -> IO CInt



