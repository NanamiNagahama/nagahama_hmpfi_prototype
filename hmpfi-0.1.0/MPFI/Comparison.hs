module Comparison where

import Internal


{-# INLINE isZero #-}
isZero   :: MPFR -> Bool
isZero (MP _ _ e _) = e == expZero --withMPFRB d mpfr_zero_p /= 0
