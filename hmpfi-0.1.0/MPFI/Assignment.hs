module Assignment where

import Internal

import Arithmetic

fromString :: String -> Precision -> MPFI
fromString v l = fst $ fromString_ v l

fromString_ :: String -> Precision -> (MPFI,Int)
fromString_ v l = withMPFI v l c_mpfi_set_str
