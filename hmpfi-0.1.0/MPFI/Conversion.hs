module Conversion where

import Internal

mpfrstring :: MPFR -> (String,Exp)
mpfrstring (MP prec sign exp man)= unsafePerformIO go
  where go = with (MP prec sign exp man) $ \p1 -> 
              alloca $ \p2 -> do
                  p3 <- c_mpfr_get_str nullPtr p2 (fromIntegral 10) (fromIntegral 0) p1 ((fromIntegral . fromEnum) Near)
                  r1 <- peekCString p3
                  r2 <- peek p2
                  c_mpfr_free_str p3
                  return (r1,r2)

toString       :: Word -> MPFR -> String
toString dec d | isInfixOf "NaN" ss = "NaN"
               | isInfixOf "Inf" ss = s ++ "Infinity"
               | otherwise          = 
                   s ++ case compare 0 e of
                          LT -> take e ss ++ 
                                (let bt = all (== '0') (drop e ss) 
                                 in if bt then "" else '.' : drop e ss)
                                ++ (if fromIntegral n - e < 0 
                                    then 'e' : show (e - fromIntegral n) 
                                    else "")
                          GT -> let ee = fromIntegral dec + e in 
                                if ee <= 0 then "0" else 
                                   head ss : '.' : (backtrim . tail . take ee) ss
                                            ++ "e" ++ show (pred e)
                          EQ -> "0." ++ let bt = all (== '0') ss 
                                        in if bt then "0" else ss
                  where (str, e') = mpfrstring d
                        n        = max dec 5
                        e = fromIntegral e'
                        (s, ss) = case head str of
                                    '-' -> ("-", tail str)
                                    _   -> ("" , str)
                        backtrim = reverse . dropWhile (== '0') . reverse

outString :: MPFR -> MPFR -> IO()
outString l r = putStrLn $ "[" ++ (show $ toString 0 l) ++ "," ++ (show $ toString 100 r) ++ "]"

-- | Output a string in base 10 rounded to Near in exponential form.
toStringExp       :: Word -- ^ number of digits
                  -> MPFR -> String
toStringExp dec d | isInfixOf "NaN" ss = "NaN"
                  | isInfixOf "Inf" ss = s ++ "Infinity"
                  | isZero d = "0"
                  | e > 0              = 
                      s ++ if Prelude.floor prec <= dec
                           then 
                               take e ss ++ 
                               let bt = backtrim (drop e ss)
                               in if null bt 
                                  then "" 
                                  else '.' : bt
                           else head ss : '.' :
                                let bt = (backtrim . tail) ss 
                                in (if null bt then "0" else bt) 
                                   ++ "e" ++ show (pred e)
                  | otherwise = 
                      s ++ (head ss : '.' : 
                               (let bt = (backtrim . tail) ss in
                                if null bt then "0" 
                                else bt )
                               ++ "e" ++ show (pred e))
                    where (str, e') = mpfrstring d
                          e = fromIntegral e'
                          n        = max dec 5
                          (s, ss) = case head str of
                                      '-' -> ("-", tail str)
                                      _   -> ("" , str)
                          backtrim = reverse . dropWhile (== '0') . reverse 
                          prec = logBase 10 2 * fromIntegral (getExp d) :: Double

outStringExp :: MPFR -> MPFR -> IO()
outStringExp l r = putStrLn $ "[" ++ (show $ toStringExp 0 l) ++ "," ++ (show $ toStringExp 0 r) ++ "]"

