{-# LANGUAGE BangPatterns #-}

module Internal (
       module FFIhelper,
       withMPFIsBA ,unsafePerformIO ,peek ,Ptr ,nullPtr ,mallocForeignPtrBytes,
       with , withForeignPtr , CInt , withCString , peekCString , alloca ,

)
where

import FFIhelper
import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Storable
import Foreign.C.Types
import Foreign.C.String
import Foreign.ForeignPtr
import System.IO.Unsafe
import Data.Int
import Data.Word
import Data.Function(on)

-- these are helper functions, only for internal use
{-# INLINE withMPFIsBA #-}
withMPFIsBA :: Precision -> MPFI -> MPFI -> (Ptr MPFI -> Ptr -> MPFI -> Ptr MPFI -> IO CInt) -> (MPFI,Int)
withMPFIsBA p !mp1 !mp2 f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               with mp2 $ \p3 ->
               f p1 p2 p3

{-# INLINE withMPFIBAui #-}
withMPFIBAui :: Precision -> MPFI -> CULong->  (Ptr MPFI -> Ptr MPFI -> CULong -> IO CInt) -> (MPFI, Int)
withMPFIBAui  p !mp1 d f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               f p1 p2 d

{-# INLINE withMPFIBAsi #-}
withMPFIBAsi ::  Precision -> MPFI -> CLong -> (Ptr MPFI -> Ptr MPFI -> CLong ->IO CInt) -> (MPFI, Int)
withMPFIBAsi  p !mp1 d f = unsafePerformIO go
    where go = withDummym p $ \ p1 ->
               with mp1 $ \ p2 ->
               f p1 p2 d

{-# INLINE withMPFIBAd #-}
withMPFIBAd  :: Precision -> MPFI -> CDouble -> (Ptr MPFI -> Ptr MPFI -> CDouble -> IO CInt) -> (MPFI, Int)
withMPFIBAd p !mp1 d f = unsafePerformIO go
    where go = withDummym p $ \ p1 ->
               with mp1 $ \ p2 ->
               f p1 p2 d

{-# INLINE withMPFIBAiu #-}
withMPFIBAiu  :: Precision -> CULong -> MPFI-> (Ptr MPFI -> CULong -> Ptr MPFI -> IO CInt)-> (MPFI, Int)
withMPFIBAiu  p d !mp1 f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               f p1 d p2

{-# INLINE withMPFIBAis #-}
withMPFIBAis   ::Precision -> CLong -> MPFI-> (Ptr MPFI -> CLong -> Ptr MPFI -> IO CInt)-> (MPFI, Int)
withMPFIBAis  p d !mp1 f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               f p1 d p2

{-# INLINE withMPFIBAd' #-}
withMPFRBAd'  :: Precision -> CDouble -> MPFI -> (Ptr MPFI -> CDouble -> Ptr MPFI -> IO CInt) -> (MPFI, Int)
withMPFRBAd' p d !mp1 f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               f p1 d p2 


{-# INLINE withMPFI #-}
withMPFI :: String -> Precision -> (Ptr MPFI -> CString -> CInt -> IO CInt) -> (MPFI, Int)
withMPFI v l f = unsafePerformIO go
  where go = withDummym l $ \p1 ->
               withCString v $ \p2 ->
                 f p1 p2 (10)

withMPFI2  ::  Precision -> MPFI -> (Ptr MPFI -> Ptr MPFI -> IO CInt) -> (MPFI, Int)
withMPFI2  p !mp1 f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               f p1 p2 
