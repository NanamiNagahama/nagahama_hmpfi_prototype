module Arithmetic
    where

import Internal

import Prelude hiding(div)

add :: Precision -> MPFI -> MPFI -> MPFI
add p d1 = fst . add_ p d1

addw :: Precision -> MPFI -> Word -> MPFI
addw  p d1 = fst . addw_  p d1

addi        ::  Precision -> MPFI -> Int -> MPFI
addi p d1 = fst . addi_  p d1

addd        ::Precision -> MPFI -> Double -> MPFI
addd p d1 = fst . addd_  p d1


sub :: Precision -> MPFI -> MPFI -> MPFI
sub p d1 = fst . sub_ p d1

subw :: Precision -> MPFI -> Word -> MPFI
subw  p d1 = fst . subw_  p d1

subi        ::  Precision -> MPFI -> Int -> MPFI
subi p d1 = fst . subi_  p d1

subd        ::Precision -> MPFI -> Double -> MPFI
subd p d1 = fst . subd_  p d1

wsub       :: Precision -> Word -> MPFI -> MPFI
wsub p d = fst . wsub_ p d

isub       ::Precision -> Int -> MPFI -> MPFI
isub  p d = fst . isub_  p d

dsub       ::  Precision -> Double -> MPFI -> MPFI
dsub p d = fst . dsub_  p d

mul :: Precision -> MPFI -> MPFI -> MPFI
mul p d1 = fst . mul_ p d1

mulw :: Precision -> MPFI -> Word -> MPFI
mulw  p d1 = fst . mulw_  p d1

muli        ::  Precision -> MPFI -> Int -> MPFI
muli p d1 = fst . muli_  p d1

muld        ::Precision -> MPFI -> Double -> MPFI
muld p d1 = fst . muld_  p d1

div :: Precision -> MPFI -> MPFI -> MPFI
div p d1 = fst . div_ p d1

divw :: Precision -> MPFI -> Word -> MPFI
divw  p d1 = fst . divw_  p d1

divi        ::  Precision -> MPFI -> Int -> MPFI
divi p d1 = fst . divi_  p d1

divd        ::Precision -> MPFI -> Double -> MPFI
divd p d1 = fst . divd_  p d1

wdiv       :: Precision -> Word -> MPFI -> MPFI
wdiv p d = fst . wdiv_ p d

idiv       ::Precision -> Int -> MPFI -> MPFI
idiv  p d = fst . idiv_  p d

ddiv       ::  Precision -> Double -> MPFI -> MPFI
ddiv p d = fst . ddiv_  p d

sqr     :: Precision -> MPFI -> MPFI 
sqr  p = fst . sqr_ p

sqrt     :: Precision -> MPFI -> MPFI
sqrt  p = fst . sqrt_  p

cbrt     ::Precision -> MPFI -> MPFI
cbrt  p = fst . cbrt_ p

neg     ::Precision -> MPFI -> MPFI
neg   p = fst . neg_  p

absD     ::Precision -> MPFI -> MPFI
absD   p = fst . abs_  p

mul2w :: Precision -> MPFI -> Word -> MPFI
mul2w p d1 = fst . mul2w_ p d1

mul2i  :: Precision -> MPFI -> Int -> MPFI
mul2i  p d1 = fst . mul2i_  p d1

div2w :: Precision -> MPFI -> Word -> MPFI
div2w p d1 = fst . div2w_ p d1

div2i  :: Precision -> MPFI -> Int -> MPFI
div2i  p d1 = fst . div2i_  p d1


add_ :: Precision -> MPFI -> MPFI -> MPFI
add_ p d1 d2 = withMPFIsBA p d1 d2 c_mpfi_add

addw_  :: Precision -> MPFI -> Word -> (MPFI, Int)
addw_  p d1 d = withMPFIBAui p d1 (fromIntegral d) c_mpfi_add_ui

addi_          :: Precision -> MPFI -> Int -> (MPFI, Int)
addi_ p d1 d = withMPFIBAsi p d1 (fromIntegral d) c_mpfi_add_si

addd_          :: Precision -> MPFI -> Double -> (MPFI, Int)
addd_ p d1 d = withMPFIBAd  p d1 (realToFrac d) c_mpfi_add_d


sub_ :: Precision -> MPFI -> MPFI -> MPFI
sub_ p d1 d2 = withMPFIsBA p d1 d2 c_mpfi_sub

subw_          :: Precision -> MPFI -> Word -> (MPFI, Int)
subw_  p d1 d = withMPFIBAui p d1 (fromIntegral d) c_mpfi_sub_ui

subi_          :: Precision -> MPFI -> Int -> (MPFI, Int)
subi_ p d1 d = withMPFIBAsi p d1 (fromIntegral d) c_mpfi_sub_si

subd_          :: Precision -> MPFI -> Double -> (MPFI, Int)
subd_ p d1 d = withMPFIBAd  p d1 (realToFrac d) c_mpfi_sub_d

wsub_          ::Precision -> Word -> MPFI -> (MPFI, Int)
wsub_  p d d1 = withMPFIBAiu p (fromIntegral d) d1 c_mpfi_ui_sub

isub_          ::Precision -> Int -> MPFI -> (MPFI, Int)
isub_  p d d1 = withMPFIBAis  p (fromIntegral d) d1 c_mpfi_si_sub

dsub_          :: Precision -> Double -> MPFI -> (MPFI, Int)
dsub_ p d d1 = withMPFRBAd' p (realToFrac d) d1 c_mpfi_d_sub

mul_ :: Precision -> MPFI -> MPFI -> MPFI
mul_ p d1 d2 = withMPFIsBA p d1 d2 c_mpfi_mul

mulw_          :: Precision -> MPFI -> Word -> (MPFI, Int)
mulw_  p d1 d = withMPFIBAui p d1 (fromIntegral d) c_m

muli_          :: Precision -> MPFI -> Int -> (MPFI, Int)
muli_ p d1 d = withMPFIBAsi p d1 (fromIntegral d) c_mpfi_mul_si

muld_          :: Precision -> MPFI -> Double -> (MPFI, Int)
muld_ p d1 d = withMPFIBAd  p d1 (realToFrac d) c_mpfi_mul_d

div_ :: Precision -> MPFI -> MPFI -> MPFI
div_ p d1 d2 = withMPFIsBA p d1 d2 c_mpfi_div

divw_          :: Precision -> MPFI -> Word -> (MPFI, Int)
divw_  p d1 d = withMPFIBAui p d1 (fromIntegral d) c_mpfi_div_ui

divi_          :: Precision -> MPFI -> Int -> (MPFI, Int)
divi_ p d1 d = withMPFIBAsi p d1 (fromIntegral d) c_mpfi_div_si

divd_          :: Precision -> MPFI -> Double -> (MPFI, Int)
divd_ p d1 d = withMPFIBAd  p d1 (realToFrac d) c_mpfi_div_d

wdiv_          ::Precision -> Word -> MPFI -> (MPFI, Int)
wdiv_  p d d1 = withMPFIBAiu p (fromIntegral d) d1 c_mpfi_ui_div

idiv_          ::Precision -> Int -> MPFI -> (MPFI, Int)
idiv_  p d d1 = withMPFIBAis  p (fromIntegral d) d1 c_mpfi_si_div

ddiv_          :: Precision -> Double -> MPFI -> (MPFI, Int)
ddiv_ p d d1 = withMPFRBAd' p (realToFrac d) d1 c_mpfi_d_div

sqr_  :: Precision -> MPFI -> (MPFI, Int)
sqr_  p d = withMPFI2  p d c_mpfi_sqr

sqrt_       :: Precision -> MPFI -> (MPFI, Int)
sqrt_  p d = withMPFI2  p d c_mpfi_sqrt

cbrt_ :: Precision -> MPFI -> (MPFI, Int)
cbrt_  p d = withMPFI2  p d c_mpfi_cbrt

neg_  :: Precision -> MPFI -> (MPFI, Int)
neg_  p d = withMPFI2  p d c_mpfi_neg

abs_  :: Precision -> MPFI -> (MPFI, Int)
abs_  p d = withMPFI2  p d c_mpfi_abs

mul2w_ :: Precision -> MPFI -> Word -> (MPFI, Int)
mul2w_ p d1 d2 = withMPFIBAui  p d1 (fromIntegral d2) mpfi_mul_2ui

mul2i_ :: Precision -> MPFI -> Int -> (MPFI, Int)
mul2i_ p d1 d2 = withMPFIBAsi p d1 (fromIntegral d2) mpfi_mul_2si

div2w_ :: Precision -> MPFI -> Word -> (MPFI, Int)
div2w_ p d1 d2 = withMPFIBAui  p d1 (fromIntegral d2) mpfi_div_2ui

div2i_ :: Precision -> MPFI -> Int -> (MPFI, Int)
div2i_ p d1 d2 = withMPFIBAsi p d1 (fromIntegral d2) mpfi_div_2si





