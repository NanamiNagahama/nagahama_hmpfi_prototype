{-# LANGUAGE ForeignFunctionInterface, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP #-}

#include <mpfr.h>
#include <chsmpfr.h>
#include <mpfi.h>
#include "nagahama.h"


module Test where

import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Storable
import Foreign.C.Types
import Foreign.C.String
import Foreign.ForeignPtr
import System.IO.Unsafe
import Data.Int
import Data.Word
import System.Environment (getArgs)
import Data.Function(on)
import Control.Monad.ST(runST,ST)


--mutableで新たに加えたもの(コメントアウトしているところはエラーが出る)
import Data.STRef(STRef, readSTRef, writeSTRef, newSTRef)

-- #if (__GLASGOW_HASKELL__ >= 702)
import Control.Monad.ST(ST)
import Control.Monad.ST.Unsafe(unsafeIOToST)
-- #else
-- import Control.Monad.ST(ST,unsafeIOToST)
-- #endif
-----


import Data.List (isInfixOf)

{- MPFRのFFIから持ってきた定義 -}
type CRoundMode = CInt
type CPrecision = #type mpfr_prec_t

data RoundMode = Near | Zero | Up | Down | MPFR_RNDNA
                 deriving (Show,Read)

instance Enum RoundMode where
#if MPFR_VERSION_MAJOR == 2
    fromEnum Near        = #{const GMP_RNDN}
    fromEnum Zero        = #{const GMP_RNDZ}
    fromEnum Up          = #{const GMP_RNDU}
    fromEnum Down        = #{const GMP_RNDD}
    fromEnum MPFR_RNDNA   = #{const GMP_RNDNA}

    toEnum #{const GMP_RNDN}    = Near
    toEnum #{const GMP_RNDZ}    = Zero
    toEnum #{const GMP_RNDU}    = Up
    toEnum #{const GMP_RNDD}    = Down
    toEnum (#{const GMP_RNDNA}) = MPFR_RNDNA
#else
    fromEnum Near        = #{const MPFR_RNDN}
    fromEnum Zero        = #{const MPFR_RNDZ}
    fromEnum Up          = #{const MPFR_RNDU}
    fromEnum Down        = #{const MPFR_RNDD}
    fromEnum MPFR_RNDNA   = #{const MPFR_RNDNA}

    toEnum #{const MPFR_RNDN}    = Near
    toEnum #{const MPFR_RNDZ}    = Zero
    toEnum #{const MPFR_RNDU}    = Up
    toEnum #{const MPFR_RNDD}    = Down
    toEnum (#{const MPFR_RNDNA}) = MPFR_RNDNA
#endif
    toEnum i                    = error $ "RoundMode.toEnum called with illegal argument :" ++ show i

instance Num Precision where
    (Precision w) + (Precision w') = Precision $ w + w'
    (Precision w) * (Precision w') = Precision $ w * w'
    (Precision a) - (Precision b) =
        if a >= b
        then Precision (a - b)
        else error $ "instance Precision Num (-): " ++
                       "Operation would result in negative precision."
    negate = error $ "instance Precision Num negate: " ++
                       "operation would result in negative precision"
    abs = id
    signum (Precision x) = Precision . signum $ x
    fromInteger i = if i >= 0
                    then Precision . fromInteger $ i
                    else error $ "instance Precision Num fromInteger: " ++
                             "operation would result  in negative precision"


instance Real Precision where
    toRational (Precision w) = toRational w

instance Integral Precision where
    quotRem (Precision w) (Precision w') = uncurry ((,) `on` Precision) $ quotRem w w'
    toInteger (Precision w) = toInteger w

{- 外部から呼び出す関数 -}

foreign import ccall unsafe "mpfi_set_str" c_mpfi_set_str ::
        Ptr MPFI -> CString -> CInt -> IO CInt
foreign import ccall unsafe "mpfr_get_str" c_mpfr_get_str ::
        CString -> Ptr Exp -> CInt -> CUInt -> Ptr MPFR -> CRoundMode -> IO CString
foreign import ccall unsafe "mpfr_custom_get_size_wrap" mpfr_custom_get_size ::
        CPrecision -> IO #{type size_t}
foreign import ccall unsafe "mpfr_free_str" c_mpfr_free_str ::
        CString -> IO ()
foreign import ccall unsafe "mpfi_add" c_mpfi_add ::
        Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt

{- MPFRから持ってきた定義　-}

newtype Precision = Precision { runPrec :: Word } deriving (Eq, Ord, Show, Enum)

type Prec = #type mpfr_prec_t
type Sign = #type mpfr_sign_t
type Exp  = #type mp_exp_t
type Limb = #type unsigned int
data MPFR = MP{ precM :: CPrecision,
                signM :: Sign,
                expM :: Exp,
                limbM :: !(ForeignPtr Limb) } deriving (Show)

instance Storable MPFR where
  sizeOf _ = #size __mpfr_struct
  alignment _ = alignment (undefined :: #{type mpfr_prec_t})
  peek = error "MPFR.peek : Not needed and not applicble"
  poke p (MP prec s e fp) = do #{poke __mpfr_struct, _mpfr_prec} p prec
                               #{poke __mpfr_struct, _mpfr_sign} p s
                               #{poke __mpfr_struct, _mpfr_exp} p e
                               withForeignPtr fp $ \p1 -> #{poke __mpfr_struct, _mpfr_d} p p1


{- MPFIの定義 -}
data MPFI = MP2{ leftM :: MPFR,
                 rightM :: MPFR } deriving (Show)

instance Storable MPFI where
  sizeOf _ = #size __mpfi_struct
  alignment _ = alignment (undefined :: #{type mpfr_prec_t}) {- 最初： #{type __mpfr_struct} -}
  peek = error "MPFI.peek : Not needed and not applicble"
  poke p (MP2 l r) = do #{poke __nagahama_struct, _mpfr_prec_l} p prec1
                        #{poke __nagahama_struct, _mpfr_sign_l} p s1
                        #{poke __nagahama_struct, _mpfr_exp_l} p e1
                        withForeignPtr fp1 $ \p1 -> #{poke __nagahama_struct, _mpfr_d_l} p p1

                        #{poke __nagahama_struct, _mpfr_prec_r} p prec2
                        #{poke __nagahama_struct, _mpfr_sign_r} p s2
                        #{poke __nagahama_struct, _mpfr_exp_r} p e2
                        withForeignPtr fp2 $ \p2 -> #{poke __nagahama_struct, _mpfr_d_r} p p2
                   where (MP prec1 s1 e1 fp1) = l
                         (MP prec2 s2 e2 fp2) = r

-- mutableで新たに定義したもの

newtype MMPFI s = MMPFI { run :: STRef s MPFI } deriving Eq

{-# INLINE unsafeWriteMMPFI #-}
-- | Replace the state of the mutable MPFR with a new one. The actual limbs are
-- not copied, so any further modifications on the mutable MPFR will reflect on
-- the MPFR given in as the second argument.
unsafeWriteMMPFI :: MMPFI s -> MPFI -> ST s ()
unsafeWriteMMPFI (MMPFI m1) m2 = writeSTRef m1 m2

{-# INLINE unsafeFreeze #-}
-- | Convert a mutable MPFR to an immutable one. The unsafe prefix comes from
-- the fact that limbs of the MPFR are not copied so any further modifications
-- on the mutable MPFR will reflect on the \"frozen\" one. If mutable MPFR will
-- not be modified afterwards, it is perfectly safe to use.
unsafeFreeze :: MMPFI s -> ST s MPFI
unsafeFreeze (MMPFI m) = readSTRef m

{-# INLINE unsafeThaw #-}
unsafeThaw :: MPFI -> ST s (MMPFI s)
unsafeThaw m = newSTRef m >>= return . MMPFI

add :: MMPFI s -> MMPFI s -> MMPFI s -> ST s Int
add = withMutableMPFIBA c_mpfi_add

{-# INLINE withMutableMPFIBA #-}
withMutableMPFIBA :: (Ptr MPFI -> Ptr MPFI -> Ptr MPFI  -> IO CInt)
                  ->  MMPFI s -> MMPFI s -> MMPFI s -> ST s Int
withMutableMPFIBA f m1 m2 m3 = do
  m1'@(MP2(MP p1 _ _ fp1)(MP p2 _ _ fp2)) <- unsafeFreeze m1
  m2' <- unsafeFreeze m2
  m3' <- unsafeFreeze m3
  (r1,(r21, r22)) <- 
    unsafeIOToST $ do
      with m3' $ \p3 -> 
          with m2' $ \p2 -> 
              with m1' $ \p1 -> do 
                     r1 <- f p1 p2 p3
                     r2 <- peekNoLimbPrec p1
                     r3 <- peekNoLimbPrec p2
                     return (fromIntegral r1, r2)
  unsafeWriteMMPFI m1 (MP2 (MP p1 r21 r22 fp1) (MP p2 r21 r22 fp2))
  return r1

----------------------------------------------------------------------



hfuncm :: String -> Precision -> MPFI
hfuncm v l = fst $ hfuncm_ v l
hfuncm_ :: String -> Precision -> (MPFI,Int)
hfuncm_ v l = withMPFI v l c_mpfi_set_str

{-# INLINE withMPFI #-}
withMPFI :: String -> Precision -> (Ptr MPFI -> CString -> CInt -> IO CInt) -> (MPFI, Int)
withMPFI v l f = unsafePerformIO go
  where go = withDummym l $ \p1 ->
               withCString v $ \p2 ->
                 f p1 p2 (10)

{-# INLINE withDummym #-}
withDummym :: Precision -> (Ptr MPFI -> IO CInt) -> IO (MPFI, Int)
withDummym l f =
  do alloca $ \ptr -> do
       ls <- mpfr_custom_get_size ((fromIntegral . runPrec ) l)
       fp1 <- mallocForeignPtrBytes (fromIntegral ls)
       fp2 <- mallocForeignPtrBytes (fromIntegral ls)
       
       #{poke __nagahama_struct, _mpfr_prec_l} ptr (fromIntegral l :: CPrecision)
     --  #{poke __nagahama_struct, _mpfr_sign_l} ptr (1 :: Sign)
     --  #{poke __nagahama_struct, _mpfr_exp_l}ptr (0 :: Exp)
       withForeignPtr fp1 $ \p1 -> #{poke __nagahama_struct,_mpfr_d_l}ptr p1

       #{poke __nagahama_struct, _mpfr_prec_r} ptr (fromIntegral l :: CPrecision)
    --   #{poke __nagahama_struct, _mpfr_sign_r} ptr (1 :: Sign)
    --   #{poke __nagahama_struct, _mpfr_exp_r}ptr (0 :: Exp)
       withForeignPtr fp2 $ \p1 -> #{poke __nagahama_struct,_mpfr_d_r}ptr p1


       r2 <- f ptr
       r1 <- peekP ptr fp1 fp2
       return(r1,fromIntegral r2)

{-# INLINE peekNoLimbPrec #-}
peekNoLimbPrec      :: Ptr MPFI -> IO (Sign,Exp)
peekNoLimbPrec p = do r11 <- #{peek __nagahama_struct, _mpfr_sign_l} p
                      r12 <- #{peek __nagahama_struct, _mpfr_exp_l} p
  
                      return (r11,r12)


{-# INLINE peekP #-}
peekP :: Ptr MPFI -> ForeignPtr Limb -> ForeignPtr Limb -> IO MPFI
peekP p fp1 fp2 = do
  r11 <- #{peek __nagahama_struct, _mpfr_prec_l}p
  r12 <- #{peek __nagahama_struct, _mpfr_sign_l}p
  r13 <- #{peek __nagahama_struct, _mpfr_exp_l}p

  r21 <- #{peek __nagahama_struct, _mpfr_prec_r}p
  r22 <- #{peek __nagahama_struct, _mpfr_sign_r}p
  r23 <- #{peek __nagahama_struct, _mpfr_exp_l}p
  
  return (MP2(MP r11 r12 r13 fp1)(MP r21 r22 r23 fp2))

mpfrstring :: MPFR -> (String,Exp)
mpfrstring (MP prec sign exp man)= unsafePerformIO go
  where go = with (MP prec sign exp man) $ \p1 -> 
              alloca $ \p2 -> do
                  p3 <- c_mpfr_get_str nullPtr p2 (fromIntegral 10) (fromIntegral 0) p1 ((fromIntegral . fromEnum) Near)
                  r1 <- peekCString p3
                  r2 <- peek p2
                  c_mpfr_free_str p3
                  return (r1,r2)

toString       :: Word -> MPFR -> String
toString dec d | isInfixOf "NaN" ss = "NaN"
               | isInfixOf "Inf" ss = s ++ "Infinity"
               | otherwise          = 
                   s ++ case compare 0 e of
                          LT -> take e ss ++ 
                                (let bt = all (== '0') (drop e ss) 
                                 in if bt then "" else '.' : drop e ss)
                                ++ (if fromIntegral n - e < 0 
                                    then 'e' : show (e - fromIntegral n) 
                                    else "")
                          GT -> let ee = fromIntegral dec + e in 
                                if ee <= 0 then "0" else 
                                   head ss : '.' : (backtrim . tail . take ee) ss
                                            ++ "e" ++ show (pred e)
                          EQ -> "0." ++ let bt = all (== '0') ss 
                                        in if bt then "0" else ss
                  where (str, e') = mpfrstring d
                        n        = max dec 5
                        e = fromIntegral e'
                        (s, ss) = case head str of
                                    '-' -> ("-", tail str)
                                    _   -> ("" , str)
                        backtrim = reverse . dropWhile (== '0') . reverse 

outString :: MPFR -> MPFR -> IO()
outString l r = putStrLn $ "[" ++ (show $ toString 100 l) ++ "," ++ (show $ toString 100 r) ++ "]"
                 
head' [] = error $ "no value supplied."
head' (h:_) = return h

main = do
  args <- getArgs
  arg0 <- head' args
  arg1 <- head' $ tail args
  arg2 <- head' $ tail (tail args)
  arg3 <- head' $ tail $ tail (tail args)
  putStrLn $ show arg0
  putStrLn $ show arg1
  let a = arg0
  let b = (read $ arg1)::Integer
  let c = arg2
  let d = (read $ arg3)::Integer

  putStrLn "FFI test_mpfi."
  let(MP2 left1 right1) = hfuncm a (fromInteger b)
  let st1 = unsafeThaw (hfuncm a (fromInteger b))
  putStrLn $ "number =" ++ (show a)
  putStrLn $ "prec = " ++ (show b)
  putStrLn $ "show(st1) : " ++ (show st1 )
  putStrLn $ "show1: " ++ (show (MP2 left1 right1))
  putStrLn $ "str1: " ++ (show $ mpfrstring left1)
  putStrLn $ "str2: " ++ (show $ mpfrstring right1)
  putStrLn $ "[str1,str2]: "
  outString left1 right1

  let(MP2 left2 right2) = hfuncm c (fromInteger d)
  let st2 = unsafeThaw (hfuncm c (fromInteger d))
  putStrLn $ "number =" ++ (show c)
  putStrLn $ "prec = " ++ (show d)
  putStrLn $ "show(st2) : " ++ (show st1 )
  putStrLn $ "show2 : " ++ (show (MP2 left2 right2))
  putStrLn $ "str1: " ++ (show $ mpfrstring left2)
  putStrLn $ "str2: " ++ (show $ mpfrstring right2)
  putStrLn $ "[str1,str2]: "
  outString left2 right2

  let st3 =  runST $ do st1' <- st1
                        st2' <- st2
                        add st1' st1' st2'
                        
                        
  putStrLn $ "show(st3) : " ++ (show st3)
  putStrLn $ "show3 : " ++ (show (MP2 left1 right1))
  putStrLn $ "number1 + number2(min): " ++ (show $ mpfrstring left1)
  putStrLn $ "number1 + number2(max): " ++ (show $ mpfrstring right1)
  putStrLn $ "[str1,str2]: "
  outString left1 right1
 

