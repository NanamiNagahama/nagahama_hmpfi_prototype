#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>
#include <stdlib.h>
#include "microsec.c"

int main(int argc, char *argv[])
{
  double start1,start2,end;
  int i;
  void microsec_(double *);
  mpfi_t a,b;
  microsec_(&start1);

  mpfi_init2 (a,atoi(argv[1]));
  mpfi_set_str (a,"1.234567",10);
  
  mpfi_init2 (b,atoi(argv[1]));
  mpfi_set_str (b,"9.876543",10);

  microsec_(&start2);
  
  for(i=0;i < 100000; i++){
  
    mpfi_add(a,a,b);
  }

  microsec_(&end);
  printf("Proceccing Time1:%lf[s]\n",end-start1);
  printf("Proceccing Time2:%lf[s]\n",end-start2);
  mpfi_out_str (stdout,10,0,a);
  return 0;

}
