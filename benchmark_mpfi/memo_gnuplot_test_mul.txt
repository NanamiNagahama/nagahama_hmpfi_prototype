set logscale x
set logscale y 2

set grid

set title 'test mul'

set ylabel 'time[s]'

set xlabel 'accuracy'

plot "result_test_mul.data" w linespoints ti 'mpfi', "result_test_mul.data" u 1:3 w linespoints ti 'hmpfi'

set term jpeg
set output "mul.jpeg"
replot
