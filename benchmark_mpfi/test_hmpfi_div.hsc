{-# LANGUAGE ForeignFunctionInterface, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}

#include <mpfr.h>
#include <chsmpfr.h>
#include <mpfi.h>
#include "nagahama.h"

module Test where

import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Storable
import Foreign.C.Types
import Foreign.C.String
import Foreign.ForeignPtr
import System.IO.Unsafe
import Data.Int
import Data.Word
import Data.Time
import System.Environment (getArgs)
import Data.Function(on)

type CRoundMode = CInt
type CPrecision = #type mpfr_prec_t

data RoundMode = Near | Zero | Up | Down | MPFR_RNDNA
                 deriving (Show,Read)

instance Enum RoundMode where
#if MPFR_VERSION_MAJOR == 2
    fromEnum Near        = #{const GMP_RNDN}
    fromEnum Zero        = #{const GMP_RNDZ}
    fromEnum Up          = #{const GMP_RNDU}
    fromEnum Down        = #{const GMP_RNDD}
    fromEnum MPFR_RNDNA   = #{const GMP_RNDNA}

    toEnum #{const GMP_RNDN}    = Near
    toEnum #{const GMP_RNDZ}    = Zero
    toEnum #{const GMP_RNDU}    = Up
    toEnum #{const GMP_RNDD}    = Down
    toEnum (#{const GMP_RNDNA}) = MPFR_RNDNA
#else
    fromEnum Near        = #{const MPFR_RNDN}
    fromEnum Zero        = #{const MPFR_RNDZ}
    fromEnum Up          = #{const MPFR_RNDU}
    fromEnum Down        = #{const MPFR_RNDD}
    fromEnum MPFR_RNDNA   = #{const MPFR_RNDNA}

    toEnum #{const MPFR_RNDN}    = Near
    toEnum #{const MPFR_RNDZ}    = Zero
    toEnum #{const MPFR_RNDU}    = Up
    toEnum #{const MPFR_RNDD}    = Down
    toEnum (#{const MPFR_RNDNA}) = MPFR_RNDNA
#endif
    toEnum i                    = error $ "RoundMode.toEnum called with illegal argument :" ++ show i

instance Num Precision where
    (Precision w) + (Precision w') = Precision $ w + w'
    (Precision w) * (Precision w') = Precision $ w * w'
    (Precision a) - (Precision b) =
        if a >= b
        then Precision (a - b)
        else error $ "instance Precision Num (-): " ++
                       "Operation would result in negative precision."
    negate = error $ "instance Precision Num negate: " ++
                       "operation would result in negative precision"
    abs = id
    signum (Precision x) = Precision . signum $ x
    fromInteger i = if i >= 0
                    then Precision . fromInteger $ i
                    else error $ "instance Precision Num fromInteger: " ++
                             "operation would result  in negative precision"


instance Real Precision where
    toRational (Precision w) = toRational w

instance Integral Precision where
    quotRem (Precision w) (Precision w') = uncurry ((,) `on` Precision) $ quotRem w w'
    toInteger (Precision w) = toInteger w

{- 外部から呼び出す関数 -}

foreign import ccall unsafe "mpfi_set_str" c_mpfi_set_str ::
        Ptr MPFI -> CString -> CInt -> IO CInt
foreign import ccall unsafe "mpfr_get_str" c_mpfr_get_str ::
        CString -> Ptr Exp -> CInt -> CUInt -> Ptr MPFR -> CRoundMode -> IO CString
foreign import ccall unsafe "mpfr_custom_get_size_wrap" mpfr_custom_get_size ::
        CPrecision -> IO #{type size_t}
foreign import ccall unsafe "mpfr_free_str" c_mpfr_free_str ::
        CString -> IO ()
foreign import ccall unsafe "mpfi_div" c_mpfi_divdiv ::
        Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt

{- MPFRから持ってきた定義　-}

newtype Precision = Precision { runPrec :: Word } deriving (Eq, Ord, Show, Enum)

type Prec = #type unsigned int
type Sign = #type unsigned int
type Exp  = #type unsigned int
type Limb = #type unsigned int
data MPFR = MP{ precM :: CPrecision,
                signM :: Sign,
                expM :: Exp,
                limbM :: !(ForeignPtr Limb) } deriving (Show)

instance Storable MPFR where
  sizeOf _ = #size __mpfr_struct
  alignment _ = alignment (undefined :: #{type mpfr_prec_t})
  peek = error "MPFR.peek : Not needed and not applicble"
  poke p (MP prec s e fp) = do #{poke __mpfr_struct, _mpfr_prec} p prec
                               #{poke __mpfr_struct, _mpfr_sign} p s
                               #{poke __mpfr_struct, _mpfr_exp} p e
                               withForeignPtr fp $ \p1 -> #{poke __mpfr_struct, _mpfr_d} p p1


{- MPFIの定義 -}
data MPFI = MP2{ leftM :: MPFR,
                 rightM :: MPFR } deriving (Show)

instance Storable MPFI where
  sizeOf _ = #size __nagahama_union
  alignment _ = alignment (undefined :: #{type mpfr_prec_t}) {- 最初： #{type __mpfr_struct} -}
  peek = error "MPFI.peek : Not needed and not applicble"
  poke p (MP2 l r) = do #{poke __nagahama_struct, _mpfr_prec_l} p prec1
                        #{poke __nagahama_struct, _mpfr_sign_l} p s1
                        #{poke __nagahama_struct, _mpfr_exp_l} p e1
                        withForeignPtr fp1 $ \p1 -> #{poke __nagahama_struct, _mpfr_d_l} p p1

                        #{poke __nagahama_struct, _mpfr_prec_r} p prec2
                        #{poke __nagahama_struct, _mpfr_sign_r} p s2
                        #{poke __nagahama_struct, _mpfr_exp_r} p e2
                        withForeignPtr fp2 $ \p2 -> #{poke __nagahama_struct, _mpfr_d_r} p p2
                   where (MP prec1 s1 e1 fp1) = l
                         (MP prec2 s2 e2 fp2) = r

divdiv :: Precision -> MPFI -> MPFI -> MPFI
divdiv p d1 = fst . divdiv_ p d1

divdiv_ :: Precision -> MPFI -> MPFI -> (MPFI,Int)
divdiv_ p d1 d2 = withMPFIsBA p d1 d2 c_mpfi_divdiv


hfuncm :: String -> Precision -> MPFI
hfuncm v l = fst $ hfuncm_ v l
hfuncm_ :: String -> Precision -> (MPFI,Int)
hfuncm_ v l = withMPFI v l c_mpfi_set_str

withMPFI :: String -> Precision -> (Ptr MPFI -> CString -> CInt -> IO CInt) -> (MPFI, Int)
withMPFI v l f = unsafePerformIO go
  where go = withDummym l $ \p1 ->
               withCString v $ \p2 ->
                 f p1 p2 (10)

withMPFIsBA :: Precision -> MPFI -> MPFI -> (Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt) -> (MPFI,Int)
withMPFIsBA p !mp1 !mp2 f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               with mp2 $ \p3 ->
               f p1 p2 p3

withDummym :: Precision -> (Ptr MPFI -> IO CInt) -> IO (MPFI, Int)
withDummym l f =
  do alloca $ \ptr -> do
       ls <- mpfr_custom_get_size ((fromIntegral . runPrec ) l)
       fp1 <- mallocForeignPtrBytes (fromIntegral ls)
       fp2 <- mallocForeignPtrBytes (fromIntegral ls)
       
       #{poke __nagahama_struct, _mpfr_prec_l} ptr (fromIntegral l :: CPrecision)
       #{poke __nagahama_struct, _mpfr_sign_l} ptr (1 :: Sign)
       #{poke __nagahama_struct, _mpfr_exp_l}ptr (0 :: Exp)
       withForeignPtr fp1 $ \p1 -> #{poke __nagahama_struct,_mpfr_d_l}ptr p1

       #{poke __nagahama_struct, _mpfr_prec_r} ptr (fromIntegral l :: CPrecision)
       #{poke __nagahama_struct, _mpfr_sign_r} ptr (1 :: Sign)
       #{poke __nagahama_struct, _mpfr_exp_r}ptr (0 :: Exp)
       withForeignPtr fp2 $ \p1 -> #{poke __nagahama_struct,_mpfr_d_r}ptr p1


       r2 <- f ptr
       r1 <- peekP ptr fp1 fp2
       return(r1,fromIntegral r2)

peekP :: Ptr MPFI -> ForeignPtr Limb -> ForeignPtr Limb -> IO MPFI
peekP p fp1 fp2 = do
  r11 <- #{peek __nagahama_struct, _mpfr_prec_l}p
  r12 <- #{peek __nagahama_struct, _mpfr_sign_l}p
  r13 <- #{peek __nagahama_struct, _mpfr_exp_l}p

  r21 <- #{peek __nagahama_struct, _mpfr_prec_r}p
  r22 <- #{peek __nagahama_struct, _mpfr_sign_r}p
  r23 <- #{peek __nagahama_struct, _mpfr_exp_l}p
  
  return (MP2(MP r11 r12 r13 fp1)(MP r21 r22 r23 fp2))

mpfrstring :: MPFR -> (String,Exp)
mpfrstring (MP prec sign exp man)= unsafePerformIO go
  where go = with (MP prec sign exp man) $ \p1 -> 
              alloca $ \p2 -> do
                  p3 <- c_mpfr_get_str nullPtr p2 (fromIntegral 10) (fromIntegral 0) p1 ((fromIntegral . fromEnum) Near)
                  r1 <- peekCString p3
                  r2 <- peek p2
                  c_mpfr_free_str p3
                  return (r1,r2)

for n a b p
  |  n > 0 = divdiv p b $ for (n-1) a b p
  |otherwise = a
   
head' [] = error $ "no dim supplied."
head' (h:_) = return h 


main = do
  args <- getArgs
  arg0 <- head' args
  let bit = (read $ arg0)::Int
  let bit' = fromIntegral bit
  
  x <- getCurrentTime

  let(MP2 left1 right1) = hfuncm "9.876543" bit'
  let(MP2 left2 right2) = hfuncm "0.1234567" bit'

  let(MP2 left3 right3) = for 100000 (MP2 left1 right1)(MP2 left2 right2) bit'
  putStrLn $ "show3 : " ++ (show (MP2 left3 right3))
  
  y <- getCurrentTime
  let time = diffUTCTime y x

  putStrLn ("Proceccing Time:" ++ show time)
