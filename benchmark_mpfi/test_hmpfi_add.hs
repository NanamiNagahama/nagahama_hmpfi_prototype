{-# LINE 1 "test_hmpfi_add.hsc" #-}
{-# LANGUAGE ForeignFunctionInterface, GeneralizedNewtypeDeriving #-}
{-# LINE 2 "test_hmpfi_add.hsc" #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MagicHash, CPP #-}


{-# LINE 6 "test_hmpfi_add.hsc" #-}

{-# LINE 7 "test_hmpfi_add.hsc" #-}

{-# LINE 8 "test_hmpfi_add.hsc" #-}

{-# LINE 9 "test_hmpfi_add.hsc" #-}

module Test where

import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Storable
import Foreign.C.Types
import Foreign.C.String
import Foreign.ForeignPtr
import System.IO.Unsafe
import Data.Int
import Data.Word
import Data.Time
import System.Environment (getArgs)
import Data.Function(on)

type CRoundMode = CInt
type CPrecision = Int64
{-# LINE 28 "test_hmpfi_add.hsc" #-}

data RoundMode = Near | Zero | Up | Down | MPFR_RNDNA
                 deriving (Show,Read)

instance Enum RoundMode where

{-# LINE 46 "test_hmpfi_add.hsc" #-}
    fromEnum Near        = 0
{-# LINE 47 "test_hmpfi_add.hsc" #-}
    fromEnum Zero        = 1
{-# LINE 48 "test_hmpfi_add.hsc" #-}
    fromEnum Up          = 2
{-# LINE 49 "test_hmpfi_add.hsc" #-}
    fromEnum Down        = 3
{-# LINE 50 "test_hmpfi_add.hsc" #-}
    fromEnum MPFR_RNDNA   = -1
{-# LINE 51 "test_hmpfi_add.hsc" #-}

    toEnum 0    = Near
{-# LINE 53 "test_hmpfi_add.hsc" #-}
    toEnum 1    = Zero
{-# LINE 54 "test_hmpfi_add.hsc" #-}
    toEnum 2    = Up
{-# LINE 55 "test_hmpfi_add.hsc" #-}
    toEnum 3    = Down
{-# LINE 56 "test_hmpfi_add.hsc" #-}
    toEnum (-1) = MPFR_RNDNA
{-# LINE 57 "test_hmpfi_add.hsc" #-}

{-# LINE 58 "test_hmpfi_add.hsc" #-}
    toEnum i                    = error $ "RoundMode.toEnum called with illegal argument :" ++ show i

instance Num Precision where
    (Precision w) + (Precision w') = Precision $ w + w'
    (Precision w) * (Precision w') = Precision $ w * w'
    (Precision a) - (Precision b) =
        if a >= b
        then Precision (a - b)
        else error $ "instance Precision Num (-): " ++
                       "Operation would result in negative precision."
    negate = error $ "instance Precision Num negate: " ++
                       "operation would result in negative precision"
    abs = id
    signum (Precision x) = Precision . signum $ x
    fromInteger i = if i >= 0
                    then Precision . fromInteger $ i
                    else error $ "instance Precision Num fromInteger: " ++
                             "operation would result  in negative precision"


instance Real Precision where
    toRational (Precision w) = toRational w

instance Integral Precision where
    quotRem (Precision w) (Precision w') = uncurry ((,) `on` Precision) $ quotRem w w'
    toInteger (Precision w) = toInteger w

{- 外部から呼び出す関数 -}

foreign import ccall unsafe "mpfi_set_str" c_mpfi_set_str ::
        Ptr MPFI -> CString -> CInt -> IO CInt
foreign import ccall unsafe "mpfr_get_str" c_mpfr_get_str ::
        CString -> Ptr Exp -> CInt -> CUInt -> Ptr MPFR -> CRoundMode -> IO CString
foreign import ccall unsafe "mpfr_custom_get_size_wrap" mpfr_custom_get_size ::
        CPrecision -> IO Word64
{-# LINE 93 "test_hmpfi_add.hsc" #-}
foreign import ccall unsafe "mpfr_free_str" c_mpfr_free_str ::
        CString -> IO ()
foreign import ccall unsafe "mpfi_add" c_mpfi_add ::
        Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt

{- MPFRから持ってきた定義　-}

newtype Precision = Precision { runPrec :: Word } deriving (Eq, Ord, Show, Enum)

type Prec = Word32
{-# LINE 103 "test_hmpfi_add.hsc" #-}
type Sign = Word32
{-# LINE 104 "test_hmpfi_add.hsc" #-}
type Exp  = Word32
{-# LINE 105 "test_hmpfi_add.hsc" #-}
type Limb = Word32
{-# LINE 106 "test_hmpfi_add.hsc" #-}
data MPFR = MP{ precM :: CPrecision,
                signM :: Sign,
                expM :: Exp,
                limbM :: !(ForeignPtr Limb) } deriving (Show)

instance Storable MPFR where
  sizeOf _ = (32)
{-# LINE 113 "test_hmpfi_add.hsc" #-}
  alignment _ = alignment (undefined :: Int64)
{-# LINE 114 "test_hmpfi_add.hsc" #-}
  peek = error "MPFR.peek : Not needed and not applicble"
  poke p (MP prec s e fp) = do (\hsc_ptr -> pokeByteOff hsc_ptr 0) p prec
{-# LINE 116 "test_hmpfi_add.hsc" #-}
                               (\hsc_ptr -> pokeByteOff hsc_ptr 8) p s
{-# LINE 117 "test_hmpfi_add.hsc" #-}
                               (\hsc_ptr -> pokeByteOff hsc_ptr 16) p e
{-# LINE 118 "test_hmpfi_add.hsc" #-}
                               withForeignPtr fp $ \p1 -> (\hsc_ptr -> pokeByteOff hsc_ptr 24) p p1
{-# LINE 119 "test_hmpfi_add.hsc" #-}


{- MPFIの定義 -}
data MPFI = MP2{ leftM :: MPFR,
                 rightM :: MPFR } deriving (Show)

instance Storable MPFI where
  sizeOf _ = (64)
{-# LINE 127 "test_hmpfi_add.hsc" #-}
  alignment _ = alignment (undefined :: Int64) {- 最初： #{type __mpfr_struct} -}
{-# LINE 128 "test_hmpfi_add.hsc" #-}
  peek = error "MPFI.peek : Not needed and not applicble"
  poke p (MP2 l r) = do (\hsc_ptr -> pokeByteOff hsc_ptr 0) p prec1
{-# LINE 130 "test_hmpfi_add.hsc" #-}
                        (\hsc_ptr -> pokeByteOff hsc_ptr 8) p s1
{-# LINE 131 "test_hmpfi_add.hsc" #-}
                        (\hsc_ptr -> pokeByteOff hsc_ptr 16) p e1
{-# LINE 132 "test_hmpfi_add.hsc" #-}
                        withForeignPtr fp1 $ \p1 -> (\hsc_ptr -> pokeByteOff hsc_ptr 24) p p1
{-# LINE 133 "test_hmpfi_add.hsc" #-}

                        (\hsc_ptr -> pokeByteOff hsc_ptr 32) p prec2
{-# LINE 135 "test_hmpfi_add.hsc" #-}
                        (\hsc_ptr -> pokeByteOff hsc_ptr 40) p s2
{-# LINE 136 "test_hmpfi_add.hsc" #-}
                        (\hsc_ptr -> pokeByteOff hsc_ptr 48) p e2
{-# LINE 137 "test_hmpfi_add.hsc" #-}
                        withForeignPtr fp2 $ \p2 -> (\hsc_ptr -> pokeByteOff hsc_ptr 56) p p2
{-# LINE 138 "test_hmpfi_add.hsc" #-}
                   where (MP prec1 s1 e1 fp1) = l
                         (MP prec2 s2 e2 fp2) = r

add :: Precision -> MPFI -> MPFI -> MPFI
add p d1 = fst . add_ p d1

add_ :: Precision -> MPFI -> MPFI -> (MPFI,Int)
add_ p d1 d2 = withMPFIsBA p d1 d2 c_mpfi_add


hfuncm :: String -> Precision -> MPFI
hfuncm v l = fst $ hfuncm_ v l
hfuncm_ :: String -> Precision -> (MPFI,Int)
hfuncm_ v l = withMPFI v l c_mpfi_set_str

withMPFI :: String -> Precision -> (Ptr MPFI -> CString -> CInt -> IO CInt) -> (MPFI, Int)
withMPFI v l f = unsafePerformIO go
  where go = withDummym l $ \p1 ->
               withCString v $ \p2 ->
                 f p1 p2 (10)

withMPFIsBA :: Precision -> MPFI -> MPFI -> (Ptr MPFI -> Ptr MPFI -> Ptr MPFI -> IO CInt) -> (MPFI,Int)
withMPFIsBA p !mp1 !mp2 f = unsafePerformIO go
    where go = withDummym p $ \p1 ->
               with mp1 $ \p2 ->
               with mp2 $ \p3 ->
               f p1 p2 p3

withDummym :: Precision -> (Ptr MPFI -> IO CInt) -> IO (MPFI, Int)
withDummym l f =
  do alloca $ \ptr -> do
       ls <- mpfr_custom_get_size ((fromIntegral . runPrec ) l)
       fp1 <- mallocForeignPtrBytes (fromIntegral ls)
       fp2 <- mallocForeignPtrBytes (fromIntegral ls)
       
       (\hsc_ptr -> pokeByteOff hsc_ptr 0) ptr (fromIntegral l :: CPrecision)
{-# LINE 174 "test_hmpfi_add.hsc" #-}
       (\hsc_ptr -> pokeByteOff hsc_ptr 8) ptr (1 :: Sign)
{-# LINE 175 "test_hmpfi_add.hsc" #-}
       (\hsc_ptr -> pokeByteOff hsc_ptr 16)ptr (0 :: Exp)
{-# LINE 176 "test_hmpfi_add.hsc" #-}
       withForeignPtr fp1 $ \p1 -> (\hsc_ptr -> pokeByteOff hsc_ptr 24)ptr p1
{-# LINE 177 "test_hmpfi_add.hsc" #-}

       (\hsc_ptr -> pokeByteOff hsc_ptr 32) ptr (fromIntegral l :: CPrecision)
{-# LINE 179 "test_hmpfi_add.hsc" #-}
       (\hsc_ptr -> pokeByteOff hsc_ptr 40) ptr (1 :: Sign)
{-# LINE 180 "test_hmpfi_add.hsc" #-}
       (\hsc_ptr -> pokeByteOff hsc_ptr 48)ptr (0 :: Exp)
{-# LINE 181 "test_hmpfi_add.hsc" #-}
       withForeignPtr fp2 $ \p1 -> (\hsc_ptr -> pokeByteOff hsc_ptr 56)ptr p1
{-# LINE 182 "test_hmpfi_add.hsc" #-}


       r2 <- f ptr
       r1 <- peekP ptr fp1 fp2
       return(r1,fromIntegral r2)

peekP :: Ptr MPFI -> ForeignPtr Limb -> ForeignPtr Limb -> IO MPFI
peekP p fp1 fp2 = do
  r11 <- (\hsc_ptr -> peekByteOff hsc_ptr 0)p
{-# LINE 191 "test_hmpfi_add.hsc" #-}
  r12 <- (\hsc_ptr -> peekByteOff hsc_ptr 8)p
{-# LINE 192 "test_hmpfi_add.hsc" #-}
  r13 <- (\hsc_ptr -> peekByteOff hsc_ptr 16)p
{-# LINE 193 "test_hmpfi_add.hsc" #-}

  r21 <- (\hsc_ptr -> peekByteOff hsc_ptr 32)p
{-# LINE 195 "test_hmpfi_add.hsc" #-}
  r22 <- (\hsc_ptr -> peekByteOff hsc_ptr 40)p
{-# LINE 196 "test_hmpfi_add.hsc" #-}
  r23 <- (\hsc_ptr -> peekByteOff hsc_ptr 16)p
{-# LINE 197 "test_hmpfi_add.hsc" #-}
  
  return (MP2(MP r11 r12 r13 fp1)(MP r21 r22 r23 fp2))

mpfrstring :: MPFR -> (String,Exp)
mpfrstring mp1 = unsafePerformIO go
  where go = with mp1 $ \p1 -> 
              alloca $ \p2 -> do
                  p3 <- c_mpfr_get_str nullPtr p2 (fromIntegral 10) (fromIntegral 100) p1 ((fromIntegral . fromEnum) Near)
                  r1 <- peekCString p3
                  r2 <- peek p2
                  c_mpfr_free_str p3
                  return (r1,r2)

--for n a b p
--  |  n > 0 = add p b $ for (n-1) a b p
--  |otherwise = a

for n !a b p
  |  n > 0 =  for (n-1) (add p a b) b p
  | otherwise = a
   
head' [] = error $ "no dim supplied."
head' (h:_) = return h 


main = do
  args <- getArgs
  arg0 <- head' args
  let bit = (read $ arg0)::Int
  let bit' = fromIntegral bit
  
  x <- getCurrentTime

--  let(MP2 left1 right1) = hfuncm "1.234567" bit'
--  let(MP2 left2 right2) = hfuncm "9.876543" bit'

--  let(MP2 left3 right3) = for 100000 (MP2 left1 right1)(MP2 left2 right2) bit'
--  putStrLn $ "show3 : " ++ (show (MP2 left3 right3))

  let a = hfuncm "1.234567" bit'
  let b  = hfuncm "9.876543" bit'

  let c  = for 800 a b  bit'
  putStrLn $ "show3 : " ++ (show c)

  
  y <- getCurrentTime
  let time = diffUTCTime y x

  putStrLn ("Proceccing Time:" ++ show time)
  let(MP2 left3 right3) = c
  putStrLn $ "show3(min) : " ++ (show $ mpfrstring left3)
  putStrLn $ "show3(min) : " ++ (show $ mpfrstring right3)
