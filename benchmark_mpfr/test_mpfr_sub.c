#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>
#include <stdlib.h>
#include "microsec.c"

int main(int argc, char *argv[])
{
  double start1,start2,end;
  int i;
  mpfr_t a,b;
  void microsec_(double *);
  
  microsec_(&start1);

  mpfr_init2 (a,atoi(argv[1]));
  mpfr_set_str (a,"987654.9876543",0,MPFR_RNDD);

  mpfr_init2 (b,atoi(argv[1]));
  mpfr_set_str (b,"1.234567",0,MPFR_RNDD);

  microsec_(&start2);
  
  for(i=0;i < 100000; i++){

    mpfr_sub(a,a,b,MPFR_RNDD);
  }

  microsec_(&end);
  printf("Proceccing Time1:%lf[s]\n",end-start1);
  printf("Proceccing Time2:%lf[s]\n",end-start2);
  //  mpfr_out_str (stdout, 10, 0,a,MPFR_RNDD);
  //  putchar ('\n');
  //  mpfr_out_str (stdout, 10, 0, b,MPFR_RNDD);
  //  putchar ('\n');

  return 0;

}
