#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>
#include <stdlib.h>
#include "microsec.c"

int main(int argc, char *argv[])
{
  double start1,start2,end;
  int i;
  void microsec_(double *);
  //  mpfr_t *c,*a,*b;
  //  a=malloc(sizeof(mpfr_t)*100000);
  //  b=malloc(sizeof(mpfr_t)*100000);
  //  c=malloc(sizeof(mpfr_t)*100000);
  mpfr_t a,b;
  microsec_(&start1);

  mpfr_init2 (a,atoi(argv[1]));
  mpfr_set_str (a,"1.234567",10,MPFR_RNDD);
  
  mpfr_init2 (b,atoi(argv[1]));
  mpfr_set_str (b,"9.876543",10,MPFR_RNDD);

  //  for(i=0;i < 100000; i++){
  //    mpfr_init2 (a[i],BIT);
  //    mpfr_set_str (a[i],"9.876543",0,MPFR_RNDD);

  //    mpfr_init2 (b[i],BIT);
  //    mpfr_set_str (b[i],"1.234567",0,MPFR_RNDD);

  //    mpfr_init2 (c[i],BIT);
  //  }
  //  start = clock();

  microsec_(&start2);
  
  for(i=0;i < 100000; i++){
  
    mpfr_add(a,a,b,MPFR_RNDD);
    // if(i%1000==0){mpfr_out_str(stdout,10,0,a,MPFR_RNDD);
    // putchar('\n');
    //    }
  }

  microsec_(&end);
  printf("Proceccing Time1:%lf[s]\n",end-start1);
  printf("Proceccing Time2:%lf[s]\n",end-start2);
  //  mpfr_out_str (stdout, 10,0,a,MPFR_RNDD);
  //  putchar ('\n');
  //  mpfr_out_str (stdout,10,0,b,MPFR_RNDD);
  //  putchar ('\n');
  return 0;

}
