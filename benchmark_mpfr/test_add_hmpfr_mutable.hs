{-# LANGUAGE BangPatterns #-}

import Data.Time
import qualified Data.Number.MPFR as M
import qualified Data.Number.MPFR.Mutable as MM
import Control.Monad.ST(runST,ST)
import System.Environment (getArgs)

s6 bit' n = runST $ do acc <- MM.unsafeThaw (M.fromString "1.234567" bit' 10)
                       b <- MM.unsafeThaw (M.fromString "9.876543" bit' 10) 
                       go n acc b
   where go 0 acc b = MM.unsafeFreeze acc
         go m acc b = do t <- MM.unsafeThaw (M.fromInt M.Near 32 m)
                         MM.add acc acc b M.Near
                         go (m-1) acc b

head' [] = error $ "no dim supplied."
head' (h:_) = return h


{-for n a b 
  |  n > 0 = M.add M.Near 100 b $ for (n-1) a b
  |otherwise = a
  -} 



main = do
  args <- getArgs
  arg0 <- head' args
  let bit = (read $ arg0)::Int
  let bit' = fromIntegral bit
  
  x <- getCurrentTime

--  let  a = M.fromString "1.234567" 100 10
--  let  b = M.fromString "9.876543" 100 10
 {- let !c = M.add M.Near 10 a b
 

  let f = \x -> return c
  
  mapM f [1..100000]-}

  
--  v <- mapM (\(a,b) -> do { return ((M.add) M.Near 10 a b)}) $ map (\x -> (a,b)) [1..100000] 

  putStrLn $ show $  s6 bit' 100000

 -- putStrLn $ (show $ head (reverse v))
--  putStrLn $ (show $ head v)

  y <- getCurrentTime
  let time = diffUTCTime y x 

  putStrLn ("Proceccing Time:" ++ show time)
