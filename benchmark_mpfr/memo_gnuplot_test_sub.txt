set logscale x
set logscale y 2

set grid

set title 'test sub'

set ylabel 'time[s]'

set xlabel 'accuracy'

plot "result_test_sub.data" w linespoints ti 'mpfr', "result_test_sub.data" u 1:3 w linespoints ti 'hmpfr' , "result_test_sub.data" u 1:4 w linespoints ti 'hmpfr mutable'

set term jpeg
set output "sub.jpeg"
replot

