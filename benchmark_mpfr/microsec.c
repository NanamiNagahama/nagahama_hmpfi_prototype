#include <sys/time.h>
#include <unistd.h>

/*double
*/
void
microsec_(double *targ)
{
        struct timeval tval_t0;
        struct timezone tzone_t0;
        double t0;


        gettimeofday(&tval_t0, &tzone_t0);
        t0 = tval_t0.tv_sec + tval_t0.tv_usec/1000000.;
/*        return t0;
*/
	*targ = t0;
}
