{-# LANGUAGE BangPatterns #-}

import Data.Time
import qualified Data.Number.MPFR as M
import System.Environment (getArgs)

for n a b p
  |  n > 0 = M.add M.Near p  b $ for (n-1) a b p
  |otherwise = a
   
head' [] = error $ "no dim supplied."
head' (h:_) = return h 


main = do
  args <- getArgs
  arg0 <- head' args
  let bit = (read $ arg0)::Int
  let bit' = fromIntegral bit
  
  x <- getCurrentTime

  let  a = M.fromString "1.234567" bit' 10
  let  b = M.fromString "9.876543" bit' 10

  putStrLn $ "a=" ++ (show a)
  putStrLn $ "b=" ++ (show b)


  {- let !c = M.add M.Near 10 a b

　
 

  let f = \x -> return c
  
  mapM f [1..100000]-}

  
--  v <- mapM (\(a,b) -> do { return ((M.add) M.Near 10 a b)}) $ map (\x -> (a,b)) [1..100000] 

  let c  =  for 100000 a b bit'
  putStrLn $ "show: " ++ show c

 -- putStrLn $ (show $ head (reverse v))
--  putStrLn $ (show $ head v)

  y <- getCurrentTime
  let time = diffUTCTime y x

  putStrLn ("Proceccing Time:" ++ show time)
  
  putStrLn $ "show(str) :" ++ (show $ M.mpfrToString M.Near 0 10 c) 
